import {
    ApolloDriver,
    ApolloDriverAsyncConfig,
    ApolloFederationDriverConfig,
} from '@nestjs/apollo'
import { ConfigService, ConfigModule } from '@nestjs/config'
import { join } from 'path'
import { configOptions } from './config'

class GraphqlOptions {
    static getGraphqlOptions(
        configService: ConfigService,
    ): ApolloFederationDriverConfig {
        return {
            autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
            sortSchema: true,
            // typePaths: ['./**/*.graphql'],
            definitions: {
                path: join(process.cwd(), 'src/graphql.ts'),
                outputAs: 'class',
            },
            debug: configService.get('GRAPHQL_DEBUG'),
            playground: configService.get('GRAPHQL_PLAYGROUND'),
            introspection: configService.get('GRAPHQL_INTROSPECTION'),
            context: ({ req }) => {
                return {
                    req,
                }
            },
        }
    }
}

export const graphqlOptionsAsync: ApolloDriverAsyncConfig = {
    driver: ApolloDriver,
    imports: [ConfigModule.forRoot(configOptions)],
    useFactory: async (configService: ConfigService) =>
        GraphqlOptions.getGraphqlOptions(configService),
    inject: [ConfigService],
}
