import { Module } from '@nestjs/common'
import { GraphQLModule } from '@nestjs/graphql'

import { AppController } from './app.controller'
import { AppService } from './app.service'
import { MatchesModule } from './matches/matches.module'
import { PrismaService } from './prisma/prisma.service'
import { graphqlOptionsAsync } from './configs/graphql.config'
import { AuthenticationService } from './authentication/services/authentication.service'
import { AuthenticationModule } from './authentication/authentication.module'
import { AuthModule } from './auth/auth.module';

@Module({
    imports: [
        GraphQLModule.forRootAsync(graphqlOptionsAsync),
        MatchesModule,
        AuthenticationModule,
        AuthModule,
    ],
    controllers: [AppController],
    providers: [AppService, PrismaService, AuthenticationService],
})
export class AppModule {}
