import { Resolver, Query, Mutation, Args } from '@nestjs/graphql'

import { User } from '../entities/User.entity'
import { PrismaService } from '../../prisma/prisma.service'
import { IStandardResponse } from '../../utils/dto/response/standard.response'
import { IRegisterAuthenticationInput } from '../dto/inputs/register-authentication.input'
import { AuthenticationService } from '../services/authentication.service'
import { ForbiddenException, UseGuards } from '@nestjs/common'
import { ILoginResponse } from '../dto/responses/login-authentication.response'
import { ILoginAuthenticationInput } from '../dto/inputs/login-authentication.input'
import { IUpdateAuthenticationInput } from '../dto/inputs/update-authentication.input'
import { IChangePasswordInput } from '../dto/inputs/change-password.input'
import { CurrentUser, GqlAuthGuard } from 'src/auth/graphql-auth.guard'
import { ICurrentUser } from '../../types'

@Resolver(() => User)
export class AuthenticationResolver {
    constructor(
        private readonly authenticationService: AuthenticationService,
    ) {}

    @UseGuards(GqlAuthGuard)
    @Query(() => User, { description: 'Fetches current logged in user' })
    current_user(@CurrentUser() current_user: ICurrentUser) {
        try {
            console.log('CURRENT_USER', current_user)
            return this.authenticationService.find_one(current_user.user_id)
        } catch (error) {
            throw new ForbiddenException()
        }
    }

    @UseGuards(GqlAuthGuard)
    @Query(() => [User], { name: 'find_all_users' })
    findAll() {
        return this.authenticationService.findAll({})
    }

    @Mutation(() => IStandardResponse)
    async register_user(
        @Args('registerUserInput') args: IRegisterAuthenticationInput,
    ) {
        try {
            await this.authenticationService.register(args)

            return {
                success: true,
                message: 'User successfully registered',
            }
        } catch (error) {
            throw new ForbiddenException({}, { description: error.message })
        }
    }

    @Mutation(() => ILoginResponse)
    login_user(@Args('loginUserInput') args: ILoginAuthenticationInput) {
        try {
            return this.authenticationService.login(args)
        } catch (error) {
            throw new ForbiddenException(error, { description: error.message })
        }
    }

    @Mutation(() => IStandardResponse)
    async change_user_password(
        @Args('changeUserPasswordInput') args: IChangePasswordInput,
    ) {
        try {
            await this.change_user_password(args)

            return {
                success: true,
                message: 'Password successfully changed',
            }
        } catch (error) {
            throw new ForbiddenException(error, { description: error.message })
        }
    }

    @Mutation(() => IStandardResponse)
    forgot_user_password() {}

    @Mutation(() => IStandardResponse)
    update_user(@Args('updateUserInput') args: IUpdateAuthenticationInput) {}
}
