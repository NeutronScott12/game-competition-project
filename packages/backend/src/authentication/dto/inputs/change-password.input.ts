import { Field, InputType } from '@nestjs/graphql'
import { IsNotEmpty } from 'class-validator'

@InputType()
export class IChangePasswordInput {
    @Field(() => String, { description: 'Email used to change password' })
    @IsNotEmpty()
    email: string

    @Field(() => String)
    @IsNotEmpty()
    password: string

    @Field(() => String)
    @IsNotEmpty()
    old_password: string
}
