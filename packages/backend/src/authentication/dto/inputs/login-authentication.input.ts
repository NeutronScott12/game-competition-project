import { Field, InputType } from '@nestjs/graphql'
import { IsEmail, IsNotEmpty } from 'class-validator'

@InputType()
export class ILoginAuthenticationInput {
    @IsEmail()
    @Field(() => String)
    email: string

    @IsNotEmpty()
    @Field(() => String)
    password: string
}
