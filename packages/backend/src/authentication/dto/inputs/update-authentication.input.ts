import { InputType, Field, Int, PartialType } from '@nestjs/graphql'

@InputType()
export class IUpdateAuthenticationInput {
    @Field(() => String, { description: 'Register Email' })
    email: string

    @Field(() => String, { description: 'Register Username' })
    username: string

    @Field(() => String, { description: 'Register Password' })
    password: string
}
