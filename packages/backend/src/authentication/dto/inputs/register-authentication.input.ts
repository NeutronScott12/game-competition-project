import { InputType, Field } from '@nestjs/graphql'
import { IsEmail, IsNotEmpty } from 'class-validator'

@InputType()
export class IRegisterAuthenticationInput {
    @IsEmail()
    @Field(() => String, { description: 'Register Email' })
    email: string

    @Field(() => String, { description: 'Register Username' })
    username: string

    @IsNotEmpty()
    @Field(() => String, { description: 'Register Password' })
    password: string
}
