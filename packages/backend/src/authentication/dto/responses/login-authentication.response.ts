import { Field, ObjectType } from '@nestjs/graphql'
import { User } from '../../../authentication/entities/User.entity'

@ObjectType()
export class ILoginResponse {
    @Field(() => Boolean)
    success: boolean

    @Field(() => String)
    token: string

    @Field(() => String)
    refresh_token: string

    @Field(() => User)
    user: User
}
