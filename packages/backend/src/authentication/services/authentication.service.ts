import { ForbiddenException, Injectable } from '@nestjs/common'
import { Prisma } from '@prisma/client'
import { compare, hash } from 'bcryptjs'
import { sign } from 'jsonwebtoken'
import { JWT_SECRET } from '../../constants'
import { errorHandler } from '../../utils/error'

import { PrismaService } from '../../prisma/prisma.service'
import { IChangePasswordInput } from '../dto/inputs/change-password.input'
import { ILoginAuthenticationInput } from '../dto/inputs/login-authentication.input'
import { IRegisterAuthenticationInput } from '../dto/inputs/register-authentication.input'
import { IUpdateAuthenticationInput } from '../dto/inputs/update-authentication.input'
import { ILoginResponse } from '../dto/responses/login-authentication.response'

@Injectable()
export class AuthenticationService {
    constructor(private readonly prisma: PrismaService) {}

    async find_one(user_id: string) {
        return this.prisma.user.findUnique({
            where: {
                id: user_id,
            },
        })
    }

    async register(args: IRegisterAuthenticationInput) {
        try {
            const { password, email, username } = args

            const hashPassword = await hash(password.trim(), 10)

            return await this.prisma.user.create({
                data: {
                    email,
                    username: username.trim(),
                    password: hashPassword,
                },
            })
        } catch (error) {
            errorHandler(error)
        }
    }

    async login(args: ILoginAuthenticationInput): Promise<ILoginResponse> {
        try {
            const { email, password } = args

            const user = await this.prisma.user.findUnique({ where: { email } })

            const isMatch = await compare(password, user.password)

            if (isMatch) {
                return {
                    success: true,
                    token: sign(
                        { user_id: user.id, username: user.username },
                        JWT_SECRET,
                    ),
                    refresh_token: sign({ id: user.id }, 'secret'),
                    user: user,
                }
            } else {
                throw new ForbiddenException()
            }
        } catch (error) {
            errorHandler(error)
        }
    }

    async change_password(args: IChangePasswordInput) {
        try {
            const { email, password, old_password } = args

            const user = await this.prisma.user.findUnique({ where: { email } })

            if (!user) {
                throw new ForbiddenException()
            }

            const isMatch = await compare(old_password, user.password)

            if (!isMatch) {
                throw new ForbiddenException()
            }

            await this.prisma.user.update({
                where: {
                    email,
                },
                data: {
                    password,
                },
            })
        } catch (error) {
            errorHandler(error)
        }
    }

    async forgotPassword() {
        try {
        } catch (error) {}
    }

    findAll(args: Prisma.UserFindManyArgs) {
        return this.prisma.user.findMany(args)
    }

    findOne(args: Prisma.UserFindUniqueArgs) {
        return this.prisma.user.findUnique(args)
    }

    update(args: Prisma.UserUpdateArgs) {
        return this.prisma.user.update(args)
    }

    remove(email: string) {
        return this.prisma.user.delete({
            where: {
                email,
            },
        })
    }
}
