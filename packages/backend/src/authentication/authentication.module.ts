import { Module } from '@nestjs/common'
import { AuthenticationService } from './services/authentication.service'
import { AuthenticationResolver } from './resolvers/authentication.resolver'
import { PrismaService } from '../prisma/prisma.service'

@Module({
    providers: [AuthenticationResolver, AuthenticationService, PrismaService],
})
export class AuthenticationModule {}
