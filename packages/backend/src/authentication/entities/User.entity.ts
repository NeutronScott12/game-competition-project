import { ObjectType, Field } from '@nestjs/graphql'

@ObjectType()
export class User {
    @Field(() => String, { description: 'User ID' })
    id: string

    @Field(() => String, { description: 'Username field' })
    username: string

    password: string

    @Field((type) => Date)
    created_at: Date

    @Field((type) => Date)
    updated_at: Date
}
