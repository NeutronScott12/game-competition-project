
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export class IChangePasswordInput {
    email: string;
    old_password: string;
    password: string;
}

export class ICreateMatchInput {
    title: string;
}

export class ILoginAuthenticationInput {
    email: string;
    password: string;
}

export class IRegisterAuthenticationInput {
    email: string;
    password: string;
    username: string;
}

export class IUpdateAuthenticationInput {
    email: string;
    password: string;
    username: string;
}

export class UpdateMatchInput {
    id: string;
    title: string;
}

export class ILoginResponse {
    refresh_token: string;
    success: boolean;
    token: string;
    user: User;
}

export class IStandardResponse {
    message: string;
    success: boolean;
}

export class Match {
    admin: User;
    admin_id: string;
    created_at: DateTime;
    id: string;
    title: string;
    updated_at: DateTime;
}

export abstract class IMutation {
    abstract change_user_password(changeUserPasswordInput: IChangePasswordInput): IStandardResponse | Promise<IStandardResponse>;

    abstract create_match(createMatchInput: ICreateMatchInput): Match | Promise<Match>;

    abstract forgot_user_password(): IStandardResponse | Promise<IStandardResponse>;

    abstract login_user(loginUserInput: ILoginAuthenticationInput): ILoginResponse | Promise<ILoginResponse>;

    abstract register_user(registerUserInput: IRegisterAuthenticationInput): IStandardResponse | Promise<IStandardResponse>;

    abstract remove_match(id: string): Match | Promise<Match>;

    abstract update_match(updateMatchInput: UpdateMatchInput): Match | Promise<Match>;

    abstract update_user(updateUserInput: IUpdateAuthenticationInput): IStandardResponse | Promise<IStandardResponse>;
}

export abstract class IQuery {
    abstract current_user(): User | Promise<User>;

    abstract find_all_users(): User[] | Promise<User[]>;

    abstract match(id: string): Match | Promise<Match>;

    abstract matches(): Match[] | Promise<Match[]>;
}

export class User {
    created_at: DateTime;
    id: string;
    updated_at: DateTime;
    username: string;
}

export type DateTime = any;
type Nullable<T> = T | null;
