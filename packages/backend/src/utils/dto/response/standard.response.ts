import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class IStandardResponse {
    @Field(() => Boolean)
    success: boolean

    @Field(() => String)
    message: string
}
