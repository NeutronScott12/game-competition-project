import { Module } from '@nestjs/common'
import { APP_GUARD } from '@nestjs/core'

import { MatchesService } from './services/matches.service'
import { MatchesResolver } from './resolvers/matches.resolver'
import { GqlAuthGuard } from '../auth/graphql-auth.guard'
import { PrismaService } from '../prisma/prisma.service'

@Module({
    providers: [
        { provide: APP_GUARD, useClass: GqlAuthGuard },
        MatchesResolver,
        MatchesService,
        PrismaService,
    ],
})
export class MatchesModule {}
