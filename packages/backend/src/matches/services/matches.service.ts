import { Injectable } from '@nestjs/common'
import { Prisma } from '@prisma/client'
import { PrismaService } from '../../prisma/prisma.service'

@Injectable()
export class MatchesService {
    constructor(private readonly prisma: PrismaService) {}

    async create_match(args: Prisma.matchCreateArgs) {
        return this.prisma.match.create(args)
    }

    async find_all_matches(args: Prisma.matchFindManyArgs) {
        return this.prisma.match.findMany(args)
    }

    async find_one_match(args: Prisma.matchFindUniqueArgs) {
        return this.prisma.match.findUnique(args)
    }

    async update_one_match(args: Prisma.matchUpdateArgs) {
        return this.prisma.match.update(args)
    }

    async remove_one_match(args: Prisma.matchDeleteArgs) {
        return this.prisma.match.delete(args)
    }
}
