import { ObjectType, Field, Int } from '@nestjs/graphql'
import { User } from '../../authentication/entities/User.entity'

@ObjectType()
export class Match {
    @Field(() => String, { description: 'Match ID' })
    id: string

    @Field(() => String)
    title: string

    @Field(() => User)
    admin: User

    @Field(() => String)
    admin_id: string

    @Field(() => Date)
    created_at: Date

    @Field(() => Date)
    updated_at: Date
}
