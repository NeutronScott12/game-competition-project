import { InputType, Field, PartialType } from '@nestjs/graphql'

@InputType()
export class UpdateMatchInput {
    @Field(() => String)
    id: string

    @Field(() => String)
    title: string
}
