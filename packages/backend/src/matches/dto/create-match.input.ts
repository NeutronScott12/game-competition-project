import { InputType, Int, Field } from '@nestjs/graphql'

@InputType()
export class ICreateMatchInput {
    @Field(() => String, { description: 'Match title' })
    title: string
}
