import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql'
import { MatchesService } from '../services/matches.service'
import { Match } from '../entities/match.entity'
import { ICreateMatchInput } from '../dto/create-match.input'
import { UpdateMatchInput } from '../dto/update-match.input'
import { CurrentUser } from '../../auth/graphql-auth.guard'
import { ICurrentUser } from '../../types'
import { InternalServerErrorException } from '@nestjs/common'

@Resolver(() => Match)
export class MatchesResolver {
    constructor(private readonly matchesService: MatchesService) {}

    @Mutation(() => Match)
    create_match(
        @Args('createMatchInput') { title }: ICreateMatchInput,
        @CurrentUser() current_user: ICurrentUser,
    ) {
        try {
            return this.matchesService.create_match({
                data: {
                    title,
                    admin: {
                        connect: {
                            id: current_user.user_id,
                        },
                    },
                },
            })
        } catch (error) {
            throw new InternalServerErrorException()
        }
    }

    @Query(() => [Match], { name: 'matches' })
    find_all_matches() {
        return this.matchesService.find_all_matches({
            include: { admin: true },
        })
    }

    @Query(() => Match, { name: 'match' })
    find_one_match(@Args('id', { type: () => String }) id: string) {
        try {
            return this.matchesService.find_one_match({
                where: {
                    id,
                },
            })
        } catch (error) {
            throw new InternalServerErrorException()
        }
    }

    @Mutation(() => Match)
    update_match(@Args('updateMatchInput') { id, title }: UpdateMatchInput) {
        try {
            return this.matchesService.update_one_match({
                where: {
                    id,
                },
                data: {
                    title,
                },
            })
        } catch (error) {
            throw new InternalServerErrorException()
        }
    }

    @Mutation(() => Match)
    remove_match(@Args('id', { type: () => String }) id: string) {
        try {
            return this.matchesService.remove_one_match({
                where: {
                    id,
                },
            })
        } catch (error) {
            throw new InternalServerErrorException()
        }
    }
}
